# Angular2 task checklist demo
A project to learn Angular2, includes the following:
- A functional demo application of tasks and checklists
- Built with Typescript
- Unit testing (more to be added)
- API docs (to be added)
- Build process, including running tests (to be added)

## Install
### Clone repository
```
git clone git@bitbucket.org:alexisbmills/ng2-task-checklist.git
```
### Install dependencies
```
npm install -g node-sass live-server typedoc
npm install
```

## Run
Run typescript converter and launch project
```
npm run tsc
npm start
```

## Run tests
Run tests with Jasmine
```
npm test
```
